import { Component, OnInit } from '@angular/core';
import {PlayerService} from '../service/player.service';
import {PointTableData} from '../module/player.module';

@Component({
  selector: 'app-point-table',
  templateUrl: './point-table.component.html',
  styleUrls: ['./point-table.component.css']
})
export class PointTableComponent implements OnInit {
  pointTable: PointTableData[] = [];

  constructor(private playerService: PlayerService) { }

  ngOnInit() {
    this.playerService.getAllPointTable().subscribe((res: PointTableData[]) => {
      this.pointTable = res;
    });
  }

}
