import {Component, OnInit} from '@angular/core';
import {PostResponce, TeamData} from '../module/player.module';
import {PlayerService} from '../service/player.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-player',
  templateUrl: './create-player.component.html',
  styleUrls: ['./create-player.component.css']
})
export class CreatePlayerComponent implements OnInit {
  allTeam: TeamData[] = [];
  logoFile: File;
  selectedTeam: TeamData;
  configAssignCarePlan = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select Team',
    limitTo: 5,
    searchPlaceholder: 'Search',
    searchOnKey: 'name'
  };
  playerForm: FormGroup;

  constructor(private playerService: PlayerService,
              private snackBar: MatSnackBar,
              private route: Router,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.playerService.getAllTeamDetails().subscribe((res: TeamData[]) => {
      this.allTeam = res;
    });
    this.refresh();
  }

  refresh() {
    this.playerForm = this.formBuilder.group({
      team: [null],
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      player_jersey_number: [null, Validators.required],
      country: [null, Validators.required]
    });
  }

  onFileSelected(image) {
    this.logoFile = image.target.files[0];
    document.getElementById('inputGroupFileLabel').innerText = this.logoFile.name;
  }

  onSelectChange(event: TeamData) {
    console.log(event);
    this.playerForm.patchValue({team: event.id});
  }

  onFormSubmit() {
    if (!this.playerForm.valid) {
      this.playerForm.markAllAsTouched();
      return;
    }
    if (this.logoFile === undefined) {
      const bar = this.snackBar.open('Please upload logo', 'X', {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top'});
      bar.onAction().subscribe(() => {
        bar.dismiss();
      });
      return;
    }
    const d = new FormData();
    d.append('team', this.playerForm.get('team').value);
    d.append('firstName', this.playerForm.get('firstName').value);
    d.append('lastName', this.playerForm.get('lastName').value);
    d.append('player_jersey_number', this.playerForm.get('player_jersey_number').value);
    d.append('country', this.playerForm.get('country').value);
    d.append('imageUri', this.logoFile);
    this.playerService.createPlayer(d).subscribe((res: PostResponce) => {
      if (res.status) {
        this.snackBar.open(res.message, 'X', {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top'});
        // this.route.navigateByUrl('/home/dashboard');
        this.playerForm.reset();
        this.playerForm.markAsUntouched();
        this.logoFile = undefined;
        document.getElementById('inputGroupFileLabel').innerText = 'Choose file';
      }
    });
  }

}
