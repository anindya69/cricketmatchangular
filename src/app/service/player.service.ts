import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PlayerProfile, PointTableData, TeamData} from '../module/player.module';

const baseUrl = 'http://127.0.0.1:8000/cricket/';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  constructor(private http: HttpClient) {
  }

  toFormData<T>(formValue: T) {
    const formData = new FormData();

    for (const key of Object.keys(formValue)) {
      const value = formValue[key];
      formData.append(key, value);
    }

    return formData;
  }

  getPlayersForTeam(teamId): Observable<any> {
    return this.http.get(`${baseUrl}team/${teamId}`).pipe();
  }

  getAllTeamDetails(): Observable<any> {
    return this.http.get<TeamData[]>(`${baseUrl}all_team/`).pipe();
  }

  getPlayerProfileById(id): Observable<any> {
    return this.http.post<PlayerProfile>(`${baseUrl}player/`, {player_id: id}).pipe();
  }

  createTeam(d): Observable<any> {
    return this.http.post(`${baseUrl}create_team/`, d).pipe();
  }

  createPlayer(d): Observable<any> {
    return this.http.post(`${baseUrl}create_player/`, d).pipe();
  }

  getAllPointTable(): Observable<any> {
    return this.http.get<PointTableData[]>(`${baseUrl}point-table/`).pipe();
  }

  playCricket(teamOne, teamTwo): Observable<any> {
    return this.http.post(`${baseUrl}play/`, {team_1: teamOne, team_2: teamTwo}).pipe();
  }
}
