import {Component, OnInit} from '@angular/core';
import {PlayerService} from '../service/player.service';
import {PlayerDetails, TeamData} from '../module/player.module';
import {MatDialog} from '@angular/material';
import {PlayerProfileComponent} from '../player-profile/player-profile.component';

@Component({
  selector: 'app-dasboard',
  templateUrl: './dasboard.component.html',
  styleUrls: ['./dasboard.component.css']
})
export class DasboardComponent implements OnInit {
  teamDetailsAll: TeamData[];
  playerDetails: PlayerDetails[] = [];

  constructor(private playerService: PlayerService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.playerService.getAllTeamDetails().subscribe((res: TeamData[]) => {
      console.log(res);
      this.teamDetailsAll = res;
    });
  }

  onClickTeam(id) {
    this.playerService.getPlayersForTeam(id).subscribe((res: PlayerDetails[]) => {
      console.log(res);
      this.playerDetails = res;
    });
  }

  onPlayerClick(id) {
    if (this.dialog.getDialogById('player-profile') === undefined) {
      this.openPlayerProfile(id);
    }
  }

  openPlayerProfile(id) {
    this.dialog.open(PlayerProfileComponent, {
      width: '400px',
      height: '600px',
      id: 'player-profile',
      data: {
        playerId: id
      }
    });
  }

}
