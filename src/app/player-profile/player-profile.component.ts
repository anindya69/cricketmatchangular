import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {PlayerService} from '../service/player.service';
import {PlayerProfile} from '../module/player.module';

@Component({
  selector: 'app-player-profile',
  templateUrl: './player-profile.component.html',
  styleUrls: ['./player-profile.component.css']
})
export class PlayerProfileComponent implements OnInit {
  playerId: number;
  playerProfile: PlayerProfile;
  constructor(public dialogRef: MatDialogRef<PlayerProfileComponent>,
              private playerService: PlayerService,
              @Inject(MAT_DIALOG_DATA) public data) {
    this.playerId = data.playerId;
  }

  ngOnInit() {
    console.log(this.playerId);
    this.playerService.getPlayerProfileById(this.playerId).subscribe((res: PlayerProfile) => {
      console.log(res);
      this.playerProfile = res;
    });
  }

}
