export interface CreateTeamData {
  name: string;
  club_state: string;
  logoUri: File;
}

export interface PostResponce {
  status: boolean;
  message: string;
}

export interface TeamData {
  id: number;
  name: string;
  club_state: string;
  logoUri: string;
}

export interface CreatePlayerData {
  team: number;
  firstName: string;
  lastName: string;
  player_jersey_number: number;
  country: string;
  imageUri: File;
}

export interface PlayerDetails {
  id: number;
  team: number;
  firstName: string;
  lastName: string;
  player_jersey_number: number;
  country: string;
  imageUri: string;
}

export interface PlayCricket {
  team_1: string;
  team_2: string;
}

export interface History {
  id: number;
  matches: number;
  run: number;
  highest_scores: number;
  fifties: number;
  hundreds: number;
  player: number;
}

export interface PlayerProfile {
  status: boolean;
  massage: string;
  data: {
    team: number;
    firstName: string;
    lastName: string;
    imageUri: string;
    player_jersey_number: number;
    country: string;
    history: History[];
  };
}

export interface PointTableData {
  team: {
    id: number;
    name: string;
    logoUri: string;
    club_state: string
  };
  point: number;
}

export interface PlayCricketResult {
  status: boolean;
  message: string;
  data: {
    team_1: string;
    team_2: string;
    winner: string;
    team_1_run: number;
    team_2_run: number;
    win_by: number;
  };
}
