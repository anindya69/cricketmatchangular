import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PlayerService} from '../service/player.service';
import {MatSnackBar} from '@angular/material';
import {PostResponce} from '../module/player.module';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-team',
  templateUrl: './create-team.component.html',
  styleUrls: ['./create-team.component.css']
})
export class CreateTeamComponent implements OnInit {
  teamForm: FormGroup;
  logoFile: File;

  constructor(private formBuilder: FormBuilder,
              private snackBar: MatSnackBar,
              private route: Router,
              private playerService: PlayerService) { }

  ngOnInit() {
    this.teamForm = this.formBuilder.group({
      name: [null, Validators.required],
      club_state: [null, Validators.required]
    });
  }

  onFileSelected(image) {
    this.logoFile = image.target.files[0];
    document.getElementById('inputGroupFileLabel').innerText = this.logoFile.name;
  }

  onFormSubmit() {
    if (!this.teamForm.valid) {
      return;
    }
    if (this.logoFile === undefined) {
      const bar = this.snackBar.open('Please upload logo', 'X', {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top'});
      bar.onAction().subscribe(() => {
        bar.dismiss();
      });
      return;
    }

    const d = new FormData();
    d.append('name', this.teamForm.get('name').value);
    d.append('club_state', this.teamForm.get('club_state').value);
    d.append('logoUri', this.logoFile);
    this.playerService.createTeam(d).subscribe((res: PostResponce) => {
      console.log(res);
      if (res.status) {
        this.snackBar.open(res.message, 'X', {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top'});
        this.route.navigateByUrl('/home/dashboard');
      }
    });
  }

}
