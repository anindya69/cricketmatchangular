import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MatSidenavModule} from '@angular/material/sidenav';

import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {CreateTeamComponent} from './create-team/create-team.component';
import {CreatePlayerComponent} from './create-player/create-player.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {PlayerProfileComponent} from './player-profile/player-profile.component';
import {PlayCricketComponent} from './play-cricket/play-cricket.component';
import {MatButtonModule, MatToolbarModule} from '@angular/material';
import {MatListModule} from '@angular/material/list';
import { DasboardComponent } from './dasboard/dasboard.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {SelectDropDownModule} from 'ngx-select-dropdown';
import { PointTableComponent } from './point-table/point-table.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateTeamComponent,
    CreatePlayerComponent,
    HomeComponent,
    PlayerProfileComponent,
    PlayCricketComponent,
    DasboardComponent,
    PointTableComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    RouterModule.forRoot([
      {path: '', redirectTo: 'home/dashboard', pathMatch: 'full'},
      {
        path: 'home', component: HomeComponent,
        children: [
          {path: 'dashboard', component: DasboardComponent},
          {path: 'create-team', component: CreateTeamComponent},
          {path: 'create-player', component: CreatePlayerComponent},
          {path: 'play', component: PlayCricketComponent},
          {path: 'point-table', component: PointTableComponent},
        ]
      },
    ]),
    MatToolbarModule,
    MatListModule,
    MatExpansionModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDialogModule,
    MatCardModule,
    FormsModule,
    MatButtonModule,
    MatSnackBarModule,
    SelectDropDownModule
  ],
  providers: [
  ],
  entryComponents: [
    PlayerProfileComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
