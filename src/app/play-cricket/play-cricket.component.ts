import {Component, OnInit} from '@angular/core';
import {PlayCricketResult, PlayerDetails, TeamData} from '../module/player.module';
import {PlayerService} from '../service/player.service';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-play-cricket',
  templateUrl: './play-cricket.component.html',
  styleUrls: ['./play-cricket.component.css']
})
export class PlayCricketComponent implements OnInit {
  allTeam: TeamData[] = [];
  selectedTeamOne: TeamData;
  selectedTeamTwo: TeamData;
  configAssignCarePlan = {
    displayKey: 'name',
    search: true,
    height: 'auto',
    placeholder: 'Select Team',
    limitTo: 5,
    searchPlaceholder: 'Search',
    searchOnKey: 'name'
  };
  result: PlayCricketResult;
  isGameDone = false;
  selectFirst = false;
  selectSecond = false;

  constructor(private playerService: PlayerService,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.playerService.getAllTeamDetails().subscribe((res: TeamData[]) => {
      this.allTeam = res;
    });
  }

  onSelectChange(event, index) {
    if (index === 1) {
      this.selectedTeamOne = event;
      this.playerService.getPlayersForTeam(this.selectedTeamOne.id).subscribe((res: PlayerDetails[]) => {
        if (res.length === 0) {
          this.selectFirst = false;
          this.snackBar.open('Please add players for Team one', 'X', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top'
          });
        } else {
          this.selectFirst = true;
        }
      });
    } else if (index === 2) {
      this.selectedTeamTwo = event;
      this.playerService.getPlayersForTeam(this.selectedTeamTwo.id).subscribe((res: PlayerDetails[]) => {
        if (res.length === 0) {
          this.selectSecond = false;
          this.snackBar.open('Please add players for Team two', 'X', {
            duration: 2000,
            horizontalPosition: 'right',
            verticalPosition: 'top'
          });
        } else {
          this.selectSecond = true;
        }
      });
    }
  }

  onClickPlay() {
    if (this.selectedTeamOne === undefined || this.selectedTeamTwo === undefined) {
      this.snackBar.open('Please select two team', 'X', {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top'});
      return;
    }
    if (this.selectedTeamOne.id === this.selectedTeamTwo.id) {
      this.snackBar.open('Please select two different team', 'X', {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top'});
      return;
    }

    if (!this.selectFirst || !this.selectSecond) {
      this.snackBar.open('Please select good team', 'X', {duration: 2000, horizontalPosition: 'right', verticalPosition: 'top'});
      return;
    }

    this.playerService.playCricket(this.selectedTeamOne.id, this.selectedTeamTwo.id).subscribe((res: PlayCricketResult) => {
      console.log(res);
      this.isGameDone = true;
      this.result = res;
    });
  }

}
